using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetObjectComponents : MonoBehaviour
{
    public GameObject go;
    // Start is called before the first frame update
    void Start()
    {
        //string text = string.Empty;
        //foreach (var component in go.GetComponentsInChildren(typeof(Component)))
        //{
        //    text = component.GetType().ToString() + " ";
        //    Debug.Log(text);
        //}

        int children = go.transform.childCount;
        string text = string.Empty;
        for (int i = 0; i < children; ++i)
        {
            //print("Child : " + go.transform.GetChild(i));

            foreach (var component in go.transform.GetChild(i).GetComponents(typeof(Component)))
            {
                text = component.GetType().ToString() + " ";
                //Debug.Log(text);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
