Shader "Custom/ShaderOutlineObject"
{
	Properties
	{
		_Color("Main Color", Color) = (0.5,0.5,0.5,0.5)
		_MainTex("Texture", 2D) = "white" {}
		_MainNormal("Normal",2D) = "white" {}
		_HighlightOutlineColor("_HighlightOutlineColor", Color) = (1,0,0,1.0)
		_HighlightOutlineWidth("_HighlightOutlineWidth", float) = 0.15
	}

		CGINCLUDE
#include "UnityCG.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			float4 normal : NORMAL;
		};

		uniform float4 _HighlightOutlineColor;
		uniform float _HighlightOutlineWidth;

		uniform sampler2D _MainTex;
		uniform sampler2D _MainNormal;
		uniform float4 _Color;

		ENDCG

			SubShader{
			//Highlight Outline
			Pass{
				Tags{ "Queue" = "Transparent" }
				Blend SrcAlpha OneMinusSrcAlpha
				ZWrite Off
				Cull Back
				CGPROGRAM

				struct v2f
				{
					float4 pos : SV_POSITION;
				};

				#pragma vertex vert
				#pragma fragment frag

				v2f vert(appdata v)
				{
					appdata original = v;

					float3 scaleOutline = normalize(v.vertex.xyz - float4(0,0,0,1));

					v.vertex.xyz += scaleOutline * _HighlightOutlineWidth;

					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					return o;
				}

				half4 frag(v2f i) : COLOR
				{
					return _HighlightOutlineColor;
				}

				ENDCG
			}

			//Surface shader
			Tags{ "Queue" = "Transparent" }

			CGPROGRAM
			#pragma surface surf Lambert noshadow

			struct Input {
				float2 uv_MainTex;
				float2 uv_MainNormal;
				float4 color : COLOR;
			};

			void surf(Input IN, inout SurfaceOutput  o) {
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = c.rgb;
				o.Normal = UnpackNormal(tex2D(_MainNormal, IN.uv_MainNormal));
				o.Alpha = c.a;
			}
			ENDCG
		}
			Fallback "Diffuse"


}