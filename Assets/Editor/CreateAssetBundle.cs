using UnityEditor;

public class CreateAssetBundle
{
    [UnityEditor.MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
        //BuildPipeline.BuildAssetBundles("../AssetBundles", BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.StandaloneOSX);
        BuildPipeline.BuildAssetBundles("../AssetBundles", BuildAssetBundleOptions.UncompressedAssetBundle, EditorUserBuildSettings.activeBuildTarget);
        //BuildPipeline.BuildAssetBundles("../AssetBundles", BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.StandaloneWindows);
    }
}
